<?php

namespace Database\Factories;

use App\Models\Jugador;
use Illuminate\Database\Eloquent\Factories\Factory;

// IMPORTANTE
use App\Models\Pais;

class JugadorFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Jugador::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'nombre' => $this->faker->name('male'),
            'numeroCamiseta' => $this->faker->numberBetween($min = 1, $max = 25),
            'fechaNacimiento' => $this->faker->dateTimeInInterval($startDate = '-35 years', $endDate = '+18 years', $timezone = null),
            'posicion' => $this->faker->randomElement($array = array ('POR','DEF','CEN','DEL')),
            'pais_id' => Pais::all()->random()->id,
        ];
    }
}
