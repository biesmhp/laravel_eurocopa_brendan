<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pais extends Model
{
    use HasFactory;
    protected $table = 'paises';

    public function getRouteKeyName()
    {
      return 'slug';
    }

    public function grupo()
    {
      return $this->belongsTo(Grupo::class, 'grupo_id');
    }

    public function jugadores()
    {
      return $this->hasMany(Jugador::class);
    }

    public function partidos()
    {
      return $this->hasMany(Partido::class, 'pais1_id')->orWhere('pais2_id', $this->id);
    }
}
