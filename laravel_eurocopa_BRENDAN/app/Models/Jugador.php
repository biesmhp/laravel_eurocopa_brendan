<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

// IMPORTANTE
use Carbon\Carbon;

class Jugador extends Model
{
    use HasFactory;
    protected $table = 'jugadores';

    public function pais()
    {
      return $this->belongsTo(Pais::class, 'pais_id');
    }

    public function getEdad()
    {
      $fechaFormateada=Carbon::parse($this->fechaNacimiento);
      return $fechaFormateada->diffInYears(Carbon::now());
    }
}
