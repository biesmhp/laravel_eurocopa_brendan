<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// IMPORTANTE
use App\Models\Partido;
use App\Models\Pais;

class PartidoController extends Controller
{
    //
    public function update(Request $a, int $id)
    {
      $partido = Partido::find($id);
        $partido->disputado = 1;
        $partido->goles_pais1 = random_int(0,4);
        $partido->goles_pais2 = random_int(0,3);
      $partido->save();
      return "El partido "+$a->pais1_id+" - "+$a->pais2_id+" se ha disputado."/*+view('paises.show', ['pais' => $pais])*/;
    }

    public function rest($partidoId)
    {

      return response()->json(['mensaje' => "Partido: "+$partidoId->pais1_id+" - "+$partidoId->pais2_id+" reseteado"]);
    }
}
