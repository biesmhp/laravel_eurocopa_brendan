<?php

use Illuminate\Support\Facades\Route;

// Importar los controladores, MUY IMPORTANTE!!
use App\Http\Controllers\GrupoController;
use App\Http\Controllers\PaisController;
use App\Http\Controllers\JugadorController;
use App\Http\Controllers\PartidoController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', [GrupoController::class,'inicio'])->name('home');

Route::get('grupos', [GrupoController::class,'index'])->name('grupos.index');

Route::get('paises/{pais}', [PaisController::class,'show'])->name('paises.show');

Route::put('partido/{partido}/disputar', [PartidoController::class,'disputar'])->name('partidos.disputar');

Route::get('api/partido/{partido}/resetear', [AnimalController::class,'resetear'])->name('partidos.resetear');
