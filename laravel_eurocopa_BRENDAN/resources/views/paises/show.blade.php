@extends('layouts.master')
@section('title')
    Eurocopa
@endsection
@section('contenido')
    <h1>Listado de paises</h1>
    <div>
      {{--TODO: Datos delanimal--}}
      <h1>{{$pais->nombre}} ({{--  --}} puntos)</h1>
    </div>
    <br>
    <div>
      <h2>Partidos</h2>
      @foreach( $pais->partidos as $partido)
        <form action="{{ route('partidos.disputar', $partido->id) }}" method="post" enctype="multipart/form-data">
          <fieldset>
            <legend>{{ $partido->pais1_id }} - {{ $partido->pais2_id }}
              <!-- Disputado -->
              <?php if ($partido->disputado==1): ?>
                {{ $partido->goles_pais1}} - {{ $partido->goles_pais2}}
              <?php else: ?>
                (No disputado)
              <?php endif; ?>
            </legend>
            <input type="submit" name="disputar" value="Jugar partido">
          </fieldset>
        </form>
      @endforeach
    </div>
    <br>
    <div>
      <h2>Jugadores</h2>
      <table border="1">
        <tr>
          <th>Número</th>
          <th>Nombre</th>
          <th>Posición</th>
          <th>Edad</th>
        </tr>
        @foreach( $pais->jugadores as $jugador)
        <tr>
          <td>{{$jugador->numeroCamiseta}}</td>
          <td>{{$jugador->nombre}}</td>
          <td>{{$jugador->posicion}}</td>
          <td>{{$jugador->getEdad()}}</td>
        </tr>
        @endforeach
      </table>
    </div>

@endsection
