@extends('layouts.master')
@section('title')
    Eurocopa
@endsection
@section('contenido')
    <h1>Listado de grupos</h1>
    <div>
      @foreach( $arrayGrupos as $grupo)
      <div>
        <h1>Grupo {{$grupo->letra}}</h1>
        <hr>
        <ul>
          @foreach( $grupo->paises as $pais)
          <li><a href="{{ route('paises.show' , $pais) }}">{{$pais->nombre}}</a>- {{--$pais->partidos->disputado->count()--}}</li>
          @endforeach
        </ul>
      </div>
      @endforeach
    </div>
@endsection
