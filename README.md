# Examen laravel_eurocopa_BRENDAN
## 15/06/2021
por Brendan Rodriguez  

Creo el proyecto  
*añado la base de datos al .env*  
Importo los seeder  
Creo las migraciones vacias  
Relleno las migraciones  
Creo los modelos  
Relleno los modelos  
*pongo los datos de faker a español*  
> php artisan migrate:fresh --seed  
FUNCIONA  

Creamos los controladores  
Añado el navbar y master proporcionado por el profesor  
Pongo las rutas  
Relleno los controladores  
Relleno las vistas  